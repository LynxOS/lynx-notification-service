from tools.DBusClient import client
from tools.IDCounter import IDCounter
from gi.repository import GLib
from dbus.mainloop.glib import DBusGMainLoop
import dbus


counter = IDCounter()
lynxNotify = None

def notificationSend(dbus, message):
    keys = ["app_name", "replaces_id", "app_icon", "summary",
            "body", "actions", "hints", "expire_timeout"]
    args = message.get_args_list()
    if len(args) == 8:
        notification = dict([(keys[i], args[i]) for i in range(8)])
        lynxNotify.addNotification(str({
            'id' : counter.getNextID(),
            'title': str(notification['summary']),
            'body': str(notification['body']),
            'icon': str(notification['app_icon'])
        }))

if __name__ == "__main__":
    DBusGMainLoop(set_as_default=True)

    lynxNotify = client('ar.net.lynx.os.desktop.service')
    notifyDBus = dbus.SessionBus()
    notifyDBus.add_match_string_non_blocking("type='method_call',interface='org.freedesktop.Notifications',member='Notify',eavesdrop=true")
    notifyDBus.add_message_filter(notificationSend)

    GLib.MainLoop().run()